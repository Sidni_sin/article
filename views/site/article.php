<?php

/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'My Yii Article';

?>

<div class="site-index">

    <div class="row">

        <div class="col-sm-12 col-md-9">
<!--            <div class="thumbnail">-->
            <div class="article">

                <img src="<?= Url::to(['images/noImg.jpg'])?>" width="100%">

                <div class="caption text-center">
                    <p class="tags">
                        <a class="tags_a" href="#">музыка</a>
                        <a class="tags_a" href="#">жизнь</a>
                        <a class="tags_a" href="#">спорт</a>
                    </p>
                    <h3>Ярлык эскиза</h3>
                    <p class="text-justify">

                        Происходит от лат. teхtus «ткань; сплетение, связь, сочетание (слов)», от texere «ткать; плести» (восходит к праиндоевр. *tek- «делать»)...
                        Происходит от лат. teхtus «ткань; сплетение, связь, сочетание (слов)», от texere «ткать; плести» (восходит к праиндоевр. *tek- «делать»)...
                    <br>
                        Происходит от лат. teхtus «ткань; сплетение, связь, сочетание (слов)», от texere «ткать; плести» (восходит к праиндоевр. *tek- «делать»)...
                        Происходит от лат. teхtus «ткань; сплетение, связь, сочетание (слов)», от texere «ткать; плести» (восходит к праиндоевр. *tek- «делать»)...
                    <br>
                        Происходит от лат. teхtus «ткань; сплетение, связь, сочетание (слов)», от texere «ткать; плести» (восходит к праиндоевр. *tek- «делать»)...

                    </p>

                </div>

            </div>
            <h3 class="text-center">Похожие статьи</h3>
            <div class="row">

                <div class="col-sm-6 col-md-3">
                    <div class="">
                        <img src="<?= Url::to(['images/noImg.jpg'])?>" width="100%">
                        <div class="caption text-center">
                            <h3>Ярлык эскиза</h3>
                            <p class="text-justify">
                                Происходит от лат. teхtus «ткань; сплетение, связь, сочетание (слов)», от texere «ткать; плести» (восходит к праиндоевр. *tek- «делать»)...
                            </p>
                            <p><a href="#" class="" role="button">Читать</a></p>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div class="col-sm-12 col-md-3">

            <div class="article-popul">
                <img src="<?= Url::to(['images/noImg.jpg'])?>" width="100%">
            </div>
            <div class="article-popul">
                <img src="<?= Url::to(['images/noImg.jpg'])?>" width="100%">
            </div>

        </div>



    </div>




</div>
